<?php

namespace App\tests;

use App\Entity\Address;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase
{

    public function testSetStreet()
    {
        $address = new Address();
        $street = 'Rue';
        $address->setStreet($street);

        $this->assertEquals($address->getStreet(), $street);
    }

    public function testSetCity()
    {
        $address = new Address();
        $city = 'Ville';
        $address->setCity($city);
        
        $this->assertEquals($address->getCity(), $city);
    }

    public function testSetCp()
    {
        $address = new Address();
        $cp = '13390';
        $address->setCp($cp);
        
        $this->assertEquals($address->getCp(), $cp);
    }

    public function testSetCp2()
    {
        $address = new Address();
        $cp = '00001';
        $address->setCp($cp);
        
        $this->assertEquals($address->getCp(), $cp);
    }
}