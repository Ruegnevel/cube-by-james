<?php

namespace App\tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class APITest extends WebTestCase
{
    public function testFirstTest()
    {
        $client = static::createClient();
        $client->request('GET', '/');

        // echo($client->getResponse());
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('title', 'Cube');
    }
}