<?php

namespace App\tests;

use App\Entity\Address;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{

    public function testSetLastname()
    {
        $user = new User();
        $lastname = 'Nom';
        $user->setLastname($lastname);

        $this->assertEquals($user->getLastname(), $lastname);
    }

    public function testSetFirstname()
    {
        $user = new User();
        $firstname = 'Prénom';
        $user->setFirstname($firstname);

        $this->assertEquals($user->getFirstname(), $firstname);
    }
}