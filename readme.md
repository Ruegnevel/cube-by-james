Lancer un test PHPUnit :
php ./vendor/bin/phpunit tests/AddressTest.php
php ./vendor/bin/phpunit tests/APITest.php

Lancer tous les tests PHPUnit :
php ./vendor/bin/phpunit

Lancer un test Cypress :
npx cypress run --spec cypress/integration/TestFonctionnel.js

Test