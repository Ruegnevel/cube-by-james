it('Test fonctionnel basique', () => {
    cy.visit('/');
    cy.contains('Se connecter').click();
    cy.get('input[name="email"]').type('citizen_1@gmail.com');
    cy.get('input[name="password"]').type('root');
    cy.contains('Confirmer').click();
    cy.contains('Bienvenue Adah Reichel').should('exist');
})