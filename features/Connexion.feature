# language: fr

Fonctionnalité: test de connexion avec un utilisateur

    Scénario: un utilisateur se connecte
    Quand je vais sur l'application
    Alors j'accède à la page d'accueil
    Quand je clique sur Se connecter
    Alors j'accède à la page de connexion
    Quand je rentre les identifiants de connexion
    Quand je clique sur Confirmer
    Alors le dashboard s'affiche
    Quand je me déconnecte
    Alors j'accède à la page de connexion