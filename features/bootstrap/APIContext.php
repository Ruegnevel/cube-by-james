<?php

namespace Tests;

use Behat\Behat\Context\Context;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class APIContext extends WebTestCase implements Context
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritDoc}
     */
    public function setUp(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    /**
     * @When j'envoie l'objet userVide.json à l'API \/app\/security\/register
     */
    public function jenvoieLobjetUservideJsonALapiAppSecurityRegister()
    {
        $client = static::createClient();

        $crawler= $client->request('GET', '/');
        
        $this->assertResponseIsSuccessful();
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        $this->em->close();
    }
}
