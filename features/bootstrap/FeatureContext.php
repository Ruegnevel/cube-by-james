<?php

use App\Entity\Address;
use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;
use PHPUnit\Framework\TestCase;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends TestCase implements Context 
{

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    // /**
    //  * @When j'envoie l'objet userVide.json à l'API \/app\/security\/register
    //  */
    // public function jenvoieLobjetUservideJsonALapiAppSecurityRegister()
    // {

    //     // $this->getSession()->visit('http://localhost:8002');
    //     // $response = $this->client->request('GET', 'http://localhost/');

    //     // $response = $this->client->request()

    //     // $response = $request->get('api_register');

    //     // echo $response;

    //     // $url = "http://localhostapp/security/register";
    //     // $data = array(
    //     //     "password" => "",
    //     //     "confirmPassword" => "",
    //     //     "email" => "",
    //     //     "lastname" => "",
    //     //     "firstname" => "",
    //     //     "address" => array(
    //     //         "street" => "",
    //     //         "city" => "",
    //     //         "cp" => "")
    //     //     );
    //     // $options = array(
    //     //     'http' => array(
    //     //         'method' => 'POST',
    //     //         'content' => http_build_query($data)
    //     //     )
    //     // );

    //     // $context = stream_context_create($options);
    //     // $result = file_get_contents($url);

    //     // var_dump($result);

    //     // throw new PendingException();

    // }


    /**
     * @Then j'ai une réponse :arg1
     */
    public function jaiUneReponse($arg1)
    {
        throw new PendingException();
        // $this->getSession()->getCurrentUrl();
    }




    /**
     * @When je vais sur l'application
     */
    public function jeVaisSurLapplication()
    {
        throw new PendingException();
    }

    /**
     * @Then j'accède à la page d'accueil
     */
    public function jaccedeALaPageDaccueil()
    {
        throw new PendingException();
    }

    /**
     * @When je clique sur Se connecter
     */
    public function jeCliqueSurSeConnecter()
    {
        throw new PendingException();
    }

    /**
     * @Then j'accède à la page de connexion
     */
    public function jaccedeALaPageDeConnexion()
    {
        throw new PendingException();
    }

    /**
     * @When je rentre les identifiants de connexion
     */
    public function jeRentreLesIdentifiantsDeConnexion()
    {
        throw new PendingException();
    }

    /**
     * @When je clique sur Confirmer
     */
    public function jeCliqueSurConfirmer()
    {
        throw new PendingException();
    }

    /**
     * @Then le dashboard s'affiche
     */
    public function leDashboardSaffiche()
    {
        throw new PendingException();
    }

    /**
     * @When je me déconnecte
     */
    public function jeMeDeconnecte()
    {
        throw new PendingException();
    }



    /**
     * @When je teste les méthodes de l'objet Address
     */
    public function jeTesteLesMethodesDeLobjetAddresse()
    {
        $address = new Address();

        $rue = 'Rue';
        $ville = 'Ville';
        $codePostal = '13390';

        $address->setStreet($rue)
            ->setCity($ville)
            ->setCp($codePostal);
        
        $this->assertEquals($address->getStreet(), $rue);
        $this->assertEquals($address->getCity(), $ville);
        $this->assertEquals($address->getCp(), $codePostal);

        // throw new PendingException();
    }
}
