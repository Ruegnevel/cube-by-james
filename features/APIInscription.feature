# language: fr


Fonctionnalité: Tests sur les attributs de l'utilisateur lors de l'inscription

    Scénario: Aucun champ rempli
    Quand j'envoie l'objet userVide.json à l'API /app/security/register
    Alors j'ai une réponse 500